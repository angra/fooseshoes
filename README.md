# Fooseshoes

Website theme based on template designed by [Enzo Li Volti](http://enzolivolti.com/).
Created by [Andrey Grachev](http://cs.angra.pw/)

[Demo](http://cs.angra.pw/templates/fooseshoes/)

## License

Use it freely but do not distribute, sell or reupload elsewhere.

## Credits

### Aller Fonts

[Aller Fonts](https://www.daltonmaag.com/) by Dalton Maag Ltd.